class Response < ActiveRecord::Base
  attr_accessible :id, :user_id, :choice_id
  validates_presence_of :user_id, :choice_id
  validate :respondent_has_not_already_answered_question
  validate :author_cannot_respond_to_own_poll

  belongs_to(
    :respondent,
    class_name: "User",
    foreign_key: :user_id,
    primary_key: :id
  )

  belongs_to(
    :answer_choice,
    class_name: "Choice",
    foreign_key: :choice_id,
    primary_key: :id
  )

  def respondent_has_not_already_answered_question
    response = existing_responses.first
    unless  response.nil? or response.id == id
      errors[:response] << "Cant answer this question again"
      return false
    end

    true
  end

  def author_cannot_respond_to_own_poll
    result = User.find_by_sql("
      SELECT
        u.*
      FROM
        users u
      JOIN
        polls p
      ON
        u.id = p.author_id
      JOIN
        questions q
      ON
        q.poll_id = p.id
      JOIN
        choices ac
      ON
        ac.question_id = q.id
      WHERE
        ac.id = #{choice_id}
      AND
        u.id = #{user_id}
        ")

    unless result.empty?
      errors[:response] << "Cant respond to own poll"
      return false
    end

    true
  end

  def existing_responses
    result = Response.find_by_sql("
      SELECT
       r.*
      FROM
        responses r
      JOIN
        choices c1
      ON
        c1.id = r.choice_id
      WHERE
        r.user_id = #{user_id}
      AND
      c1.question_id =
      (
        SELECT
          question_id
        FROM
          choices c2
        WHERE
          c2.id = #{choice_id}
      )
    ")
    result
  end

end