class Question < ActiveRecord::Base
  attr_accessible :text, :id, :poll_id
  validates_presence_of :text, :poll_id

  belongs_to(
    :poll,
    class_name: "Poll",
    foreign_key: :poll_id,
    primary_key: :id
  )

  has_many(
    :answer_choices,
    class_name: "Choice",
    foreign_key: :question_id,
    primary_key: :id
  )


  def results
    result = ActiveRecord::Base.connection.execute("
      SELECT
        ac.text AS answer, COUNT(r.user_id) AS num
      FROM
        questions q
      JOIN
        choices ac
      ON
        q.id = ac.question_id
      JOIN
        responses r
      ON
        r.choice_id = ac.id
      WHERE
        q.id = #{id}
      GROUP BY
        ac.id

      ")

      p result
  end

end