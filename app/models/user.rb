class User < ActiveRecord::Base
  attr_accessible :name
  validates_presence_of :name
  validates_uniqueness_of :name

  has_many(
    :authored_polls,
    class_name: "Poll",
    foreign_key: :author_id,
    primary_key: :id
  )

  has_many(
    :responses,
    class_name: "Response",
    foreign_key: :user_id,
    primary_key: :id
  )

  def completed_polls

    result = Poll.find_by_sql("
    SELECT
      distinct p.*
      FROM
      polls p
      JOIN
      questions q
      ON
      p.id = q.poll_id
      WHERE
      (SELECT
      count(questions.id)
      FROM
      questions
      WHERE
      questions.poll_id = p.id)
        =
      (SELECT
      count(r.id)
      FROM
      responses r
      JOIN
      choices ac
      ON
      r.choice_id = ac.id
      JOIN
      questions q2
      ON
      q2.id = ac.question_id
      JOIN
      polls
      ON
      polls.id = q2.poll_id
      WHERE
      polls.id = p.id
      AND
      r.user_id = #{id}
      ) ")


  end

  def incomplete_polls
    result = Poll.find_by_sql("
    SELECT
      distinct p.*
      FROM
      polls p
      JOIN
      questions q
      ON
      p.id = q.poll_id
      WHERE
      (SELECT
      count(r.id)
      FROM
      responses r
      JOIN
      choices ac
      ON
      r.choice_id = ac.id
      JOIN
      questions q2
      ON
      q2.id = ac.question_id
      JOIN
      polls
      ON
      polls.id = q2.poll_id
      WHERE
      polls.id = p.id
      AND
      r.user_id = #{id}
      )
      BETWEEN
      0
      AND
      ((SELECT
      count(questions.id)
      FROM
      questions
      WHERE
      questions.poll_id = p.id)-1)
       ")
    end

end

