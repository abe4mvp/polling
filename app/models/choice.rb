class Choice < ActiveRecord::Base
  attr_accessible :text, :id, :question_id
  validates_presence_of :text, :question_id

  has_many(
    :responses,
    class_name: "Responce",
    foreign_key: :choice_id,
    primary_key: :id
  )

  belongs_to(
    :question,
    class_name: "Question",
    foreign_key: :question_id,
    primary_key: :id
  )

end