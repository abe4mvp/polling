# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


#already executed
# users = User.create!([
#   {name: "Dan"},
#   {name: "Abe"}
# ])

dan = User.find_by_name("Dan")
abe = User.find_by_name("Abe")


# polls = Poll.create!([
#   { author_id: dan.id,
#     title: "Favorite Food"},
#   { author_id: abe.id,
#     title: "Sports"}
# ])