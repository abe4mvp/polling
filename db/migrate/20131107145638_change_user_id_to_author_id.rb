class ChangeUserIdToAuthorId < ActiveRecord::Migration
  def change
    rename_column :polls, :user_id, :author_id
  end


end
