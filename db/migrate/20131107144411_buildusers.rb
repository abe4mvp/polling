class Buildusers < ActiveRecord::Migration
  def change
    create_table :users, :primary_key => :id do |t|
      t.string :name
      t.integer :id

      t.timestamps
    end
  end
end
