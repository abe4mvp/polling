class Buildpolls < ActiveRecord::Migration
  def change
    create_table :polls, :primary_key => :id do |t|
      t.string :title
      t.integer :id

      t.integer :user_id
      
      t.timestamps
    end
  end
end
