class Buildquestions < ActiveRecord::Migration
  def change
    create_table :questions, :primary_key => :id do |t|
      t.string :text
      t.integer :id

      t.integer :poll_id
      
      t.timestamps
    end
  end
end
