class Buildresponses < ActiveRecord::Migration
  def change
    create_table :responses, :primary_key => :id do |t|
      t.integer :id

      t.integer :user_id
      t.integer :choice_id
      t.integer :question_id
      t.timestamps
    end
  end
end
