class Buildchoices < ActiveRecord::Migration
  def change
    create_table :choices, :primary_key => :id do |t|
      t.string :text
      t.integer :id

      t.integer :question_id
      
      t.timestamps
    end
  end
end
